package com.example.j;

public class Person {

    String name;

    String email;

    String personaddress;


    public Person(String name, String email,String personaddress) {
        this.name = name;
        this.email = email;
        this.personaddress = personaddress;
    }

    public Person(String name, String email) {
        this.name = name;
        this.email = email;
        this.personaddress = "";
    }


    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getPersonaddress() {
        return personaddress;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPersonaddress(String personaddress) {
        this.personaddress = personaddress;
    }
}
