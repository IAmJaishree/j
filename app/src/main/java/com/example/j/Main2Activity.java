package com.example.j;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.FrameLayout;

public class Main2Activity extends AppCompatActivity {

    FrameLayout frameLayout;
    FragmentManager fragmentManager;
    Fragment fragment;
    FragmentTransaction fragmentTransaction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        frameLayout = findViewById(R.id.fragment_container);

        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        goToFirstFragment();
    }

    public void goToFirstFragment() {

        Fragment fragment = fragmentManager.findFragmentById(R.id.fragment_container);
        if (fragment != null) {
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.remove(fragment);
            fragmentTransaction.commit();
        }


        fragmentTransaction = fragmentManager.beginTransaction();
        Fragment newFragment = new Main2ActivityFragment();
        fragmentTransaction.add(R.id.fragment_container, newFragment);
        fragmentTransaction.commit();
    }


    public void goToSecondFragment() {

        

        Fragment fragment = fragmentManager.findFragmentById(R.id.fragment_container);

        if (fragment != null) {
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.remove(fragment);
            fragmentTransaction.commit();
        }
        fragmentTransaction = fragmentManager.beginTransaction();
        Fragment newFragment = new HomeFragment();
        fragmentTransaction.add(R.id.fragment_container, newFragment);
        fragmentTransaction.commit();


    }
}
