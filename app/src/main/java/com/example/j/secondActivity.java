package com.example.j;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.ArrayList;

public class secondActivity extends AppCompatActivity {

    RecyclerView recyclerView;

    PersonAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        recyclerView = findViewById(R.id.recyclerview);



        ArrayList<Person> p = new ArrayList<>();

        for(int i= 0 ; i <100; i ++){
            p.add(new Person("jaishree " + i, "jaishree@gmail.com"));
        }



        adapter = new PersonAdapter(p);
        recyclerView.setAdapter(adapter);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mLayoutManager);



//        recyclerView.getAdapter().notifyDataSetChanged();

    }

    public void vishwakarma(View view) {
    }
}
