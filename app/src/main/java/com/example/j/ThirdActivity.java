package com.example.j;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.ArrayList;

public class ThirdActivity extends AppCompatActivity {

    RecyclerView shopRecyclerView,personRecyclerView;

    PersonAdapter personAdapter;
    ShopAdapter shopAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third);

        personRecyclerView = findViewById(R.id.recyclerview);
        shopRecyclerView = findViewById(R.id.recyclerviewA);


        ArrayList<Shops> s = new ArrayList<>();




        for(int i= 0 ; i<50 ; i++){
            Shops shops =new Shops("shree"+i,"qwerty");


            s.add(shops);
        }
        ArrayList<Person> p = new ArrayList<>();

        for(int i= 0 ; i <100; i ++){
            p.add(new Person("jaishree " + i, "jaishree@gmail.com"));
        }
        shopAdapter = new ShopAdapter(s);
        shopRecyclerView.setAdapter(shopAdapter);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        shopRecyclerView.setLayoutManager(mLayoutManager);


        personAdapter = new PersonAdapter(p);
        personRecyclerView.setAdapter(personAdapter);

        RecyclerView.LayoutManager mLayoutManagerTwo = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        personRecyclerView.setLayoutManager(mLayoutManagerTwo);

    }

    public void vishwakarma(View view) {
    }
}
