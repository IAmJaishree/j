package com.example.j;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;


public class ShopAdapter extends RecyclerView.Adapter<ShopAdapter.MyViewHolder> {

    private List<Shops> shopsList;
    private Context context;

    public ShopAdapter(List<Shops> shopsList) {
        this.shopsList = shopsList;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        this.context = parent.getContext();
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.shops, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ShopAdapter.MyViewHolder myViewHolder, int i) {
        myViewHolder.bind(i);
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView shopimageView;
        TextView shopnameView;
        TextView addressView;

        public MyViewHolder(View view) {
            super(view);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Shops shops = shopsList.get(getPosition());
                }
            });

            shopimageView = view.findViewById(R.id.shopimage);
            shopnameView = view.findViewById(R.id.shopname);
            addressView = view.findViewById(R.id.shopaddress);
        }

        public void bind(int position) {
            Shops shops = shopsList.get(position);
            shopimageView.setBackground(context.getResources().getDrawable(R.drawable.ic_launcher_background));
            shopnameView.setText(shops.getShopname());
            addressView.setText(shops.getAddress());
        }
    }

    @Override
    public int getItemCount() {
        return shopsList.size();
    }
}