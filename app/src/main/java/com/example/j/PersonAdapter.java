package com.example.j;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;


public class PersonAdapter extends RecyclerView.Adapter<PersonAdapter.MyViewHolder> {

    private List<Person> personList;
    Context context;

    public PersonAdapter(List<Person> personList) {
        this.personList = personList;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.person_view, parent, false);
        return new MyViewHolder(itemView);
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {


        ImageView personimageView;
        TextView nameView;
        TextView emailView;
        TextView personaddressView;

        public MyViewHolder(View view) {
            super(view);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, ThirdActivity.class);
                    context.startActivity(intent);
                }
            });
            personimageView = view.findViewById(R.id.personImage);
            nameView = view.findViewById(R.id.personName);
            emailView = view.findViewById(R.id.personemail);
            personaddressView = view.findViewById(R.id.addresstext);
        }

        public void bind(int position) {
            Person person = personList.get(position);
            personimageView.setBackground(context.getResources().getDrawable(R.drawable.ic_launcher_background));
            nameView.setText(person.getName());
            emailView.setText(person.getEmail());
            personaddressView.setText(person.getPersonaddress());
        }
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        return personList.size();
    }
}