package com.example.j.FourTabActivity;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;



class PagerAdapter2 extends FragmentPagerAdapter {
    public PagerAdapter2(FragmentManager fm1) {
        super(fm1);

    }

    @Override
    public Fragment getItem(int i) {
        if(i==0)
            return new fragment1();
        //if(i==1)
           // return new fragment2();
        //if (i==2)
           // return new fragment3();

       return new fragment2();
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return ("Tab : "+ position);
    }
}
