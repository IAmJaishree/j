package com.example.j.FourTabActivity;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.j.R;

public class NameActivity extends AppCompatActivity {
    TabLayout tabLayout;
    ViewPager viewPager;
    android.support.v4.view.PagerAdapter pagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_name);

        tabLayout = findViewById(R.id.tablayout);
        viewPager = findViewById(R.id.view_pager);
        pagerAdapter = new PagerAdapter2(getSupportFragmentManager());

        viewPager.setAdapter(pagerAdapter);

        tabLayout.setupWithViewPager(viewPager);

    }

    public void changePAge(int page){
        viewPager.setCurrentItem(page);
    }
}
