package com.example.j;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

public class ConstraintActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    PersonAdapter personAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_constraint);

        recyclerView = findViewById(R.id.personRecyclerView);
        ArrayList<Person> p = new ArrayList<>();

        for(int i= 0 ; i <100; i ++){
            p.add(new Person("jaishree " + i, "jaishree@gmail.com"));
        }
        personAdapter = new PersonAdapter(p);
        recyclerView.setAdapter(personAdapter);

        RecyclerView.LayoutManager mLayoutManagerTwo = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mLayoutManagerTwo);

    }

}
